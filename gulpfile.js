var gulp = require('gulp'),
	sass = require('gulp-sass'),
	browserSync = require('browser-sync'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglifyjs'),
	cssnano = require('gulp-cssnano'),
	rename = require('gulp-rename'),
	del = require('del'),
	cache = require('gulp-cache'),
	autoprefixer = require('gulp-autoprefixer'),
	plumber = require('gulp-plumber');

gulp.task('sass', function() {
	return gulp.src([
        'app/sass/main.scss'
	])
    .pipe(plumber())
	.pipe(sass())
	.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'],{ cascade: true }))
	.pipe(concat('main.css'))
	.pipe(gulp.dest('app/css/'))
	.pipe(browserSync.reload({stream: true}))
});

gulp.task('css-libs', function() {
    return gulp.src('app/sass/libs/**/*')
        .pipe(plumber())
        .pipe(sass())
        .pipe(concat('libs.css'))
		.pipe(cssnano())
		.pipe(rename('libs.min.css'))
        .pipe(gulp.dest('app/css/'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('scripts', function () {
	return gulp.src([
			'app/libs/jquery/dist/jquery.min.js',
			'libs/fancybox-master/dist/jquery.fancybox.min.js',
			'libs/slick/slick.min.js',
			'libs/bootstrap/js/bootstrap.bundle.min.js',
		])
	.pipe(concat('libs.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('app/js/'))
});

gulp.task('browser-sync', function(){
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false
	});
});

gulp.task('clean', function () {
	return del.sync('dist/');
});

gulp.task('clear', function () {
	return cache.clearAll();
});

gulp.task('watch', ['browser-sync', 'sass','css-libs', 'scripts'], function() {
	gulp.watch('app/sass/**/*', ['sass']);
	gulp.watch('app/*.html', browserSync.reload);
	gulp.watch('app/js/**/*.js', browserSync.reload);
});



gulp.task('build', ['clean', 'sass', 'scripts'], function () {
	var buildCss = gulp.src([
		'app/css/main.css'
	])
	.pipe(gulp.dest('dist/css'));

	var buildFonts = gulp.src('app/fonts/**/*')
		.pipe(gulp.dest('dist/fonts'));

	var buildJs = gulp.src('app/js/**/*')
		.pipe(gulp.dest('dist/js'));

	var buildHtml = gulp.src('app/*.html')
		.pipe(gulp.dest('dist/'));
});

